//type assertion vs type conversion (casting)

package main

import "fmt"

func main() {

	//type assertion must be used when dealing with interfaces
	var val interface{} = 7
	fmt.Printf("%T\n", val)
	fmt.Printf("%T\n", val.(int))

	//type conversion must be used for everything else
	rem := 7.24
	fmt.Printf("%T\n", rem)
	fmt.Printf("%T\n", int(rem))
}
