/*
Interfaces are implemented implicitly.
A TYPE implements an INTERFACE by implementing its methods.
There is no explicit declaration of intent, no "impliments" keyword.
Implicit interfaces decouple the definition of an interface from its
implementation which could then appear in any package without prearrangement.
*/

package main

import (
	"fmt"
	"math"
)

// Square defines a struct
type Square struct {
	side float64
}

// Circle defines a struct
type Circle struct {
	radius float64
}

// Rectangle defines a struct
type Rectangle struct {
	length float64
	width  float64
}

// Shape sets up an interface to be used with shapes
// anything that has either of the methods below is implementing the Shape interface
type Shape interface {
	area() float64
	perimeter() float64
}

// when you call area with a Square this function is used
func (s Square) area() float64 {
	return s.side * s.side
}

// when you call perimeter with a Square this function is used
func (s Square) perimeter() float64 {
	return s.side * 4
}

// when you call area with a Circle this function is used
func (c Circle) area() float64 {
	return math.Pi * c.radius * c.radius
}

// when you call perimeter with a Circle this function is used
func (c Circle) perimeter() float64 {
	return 2 * math.Pi * c.radius
}

// when you call area with a Rectangle this function is used
func (r Rectangle) area() float64 {
	return r.length * r.width
}

// when you call perimeter with a Rectangle this function is used
func (r Rectangle) perimeter() float64 {
	return r.length*2 + r.width*2
}

// info will be run the Shape interface methods on all s
func info(z Shape) {
	fmt.Printf("%T\n", z)      // Print z type
	fmt.Println(z)             // Print z parameters
	fmt.Println(z.area())      // Print z area
	fmt.Println(z.perimeter()) // Print z perimeter
}

func main() {
	// declare shapes and parameters
	s := Square{side: 10}
	c := Circle{radius: 5}
	r := Rectangle{length: 3, width: 5}
	// call info() on all shapes
	info(s)
	info(c)
	info(r)
}
