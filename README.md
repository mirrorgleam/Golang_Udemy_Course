# **[Golang Course Link](https://www.udemy.com/learn-how-to-code/)**

## Lessons
- [x] 01 - Introduction
- [x] 02 - Installing Go
- [x] 03 - Your Dev Environment
- [x] 04 - Computer Fundamentals
- [x] 05 - Language Fundamentals
- [x] 06 - Control Flow
- [x] 07 - Functions
- [x] 08 - Data Structures: Array
- [x] 09 - Data Structures: Slice
- [x] 10 - Data Structures: Map
- [x] 11 - Data Structures: Struct
- [x] 12 - Interfaces
- [ ] 13 - Concurrency
- [ ] 14 - Channels
- [ ] 15 - Applied Concurrency
- [ ] 16 - Concurrency Challenges
- [ ] 17 - Concurrency Resources
- [ ] 18 - Error Handling
- [ ] 19 - Farewell




## List of useful Golang resources

* Online Resources:
    * [Instructor github page](https://github.com/GoesToEleven/GolangTraining)
    
    * [Go By Example](https://gobyexample.com)
    
    * [Go Standard library](https://golang.org/pkg/#stdlib)

    * [Go lib search](https://godoc.org/)

* Books:
    * Paper: 
        * [Go in Action](https://www.amazon.com/gp/product/1617291781/ref=ox_sc_act_image_1?smid=ATVPDKIKX0DER&psc=1)
        
        * [The Go Programming Language](https://www.amazon.com/Programming-Language-Addison-Wesley-Professional-Computing/dp/0134190440/ref=sr_1_2?s=books&ie=UTF8&qid=1517931150&sr=1-2&keywords=the+go+programming+language+second+edition&refinements=p_n_availability%3A2245265011)
    
    * Online:
        * [Go in Action](https://github.com/custa/it-ebooks/blob/master/programming/go/Go%20in%20Action.pdf)
        
        * [An Introduction to Programming in Go](https://www.golang-book.com/)

            