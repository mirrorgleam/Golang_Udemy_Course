package main

import (
	"fmt"
)

// byValue: Copies the variable to be modified in the scope of the function but no where else
func byValue(z string) {
	fmt.Println("\nbyValue func returns:")
	fmt.Println("Value of z before change -", z)
	fmt.Println("Address of z before change -", &z)
	z = "Modified copy of data"
	fmt.Println("Value of z after change -", z)
	fmt.Println("Address of z after change -", &z)
}

// byPointer: Accesses the memory address directly to modify the associated value
func byPointer(z *string) {
	fmt.Println("\nbyPointer func returns:")
	fmt.Println("Value of z before change -", *z)
	fmt.Println("Address of z before change -", &z)
	*z = "Modified value at x address"
	fmt.Println("Value of z after change -", *z)
	fmt.Println("Address of z after change -", &z)

}

func main() {

	x := "initial string"

	fmt.Println("Initial value of x -", x)
	fmt.Println("Initial address of x -", &x)

	byValue(x)

	fmt.Println("\nIn main() After byValue()")
	fmt.Println("Value of x -", x)
	fmt.Println("Address of x after byValue() -", &x)

	byPointer(&x)

	fmt.Println("\nIn main() After byPointer()")
	fmt.Println("Value of x -", x)
	fmt.Println("Address of x after byPointer() -", &x)

}
