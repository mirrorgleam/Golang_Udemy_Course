package main

import (
	"fmt"
)

func main() {

	a := 42

	fmt.Println(a)  // 42
	fmt.Println(&a) // hex memory address for 42

	var b = &a // assign var b to be a pointer to the memory address of var a

	fmt.Println(b)  // print the memory address stored in var b
	fmt.Println(*b) // print the value at the memory address stored in var b (dereference)
	// var b is of type int pointer (*int)
	// var b points to the memory address where an int is stored
	// to retrive the value of that memory address use *b to dereference

	// to directly modify values at memory addresses
	// we can pass a dereferenced memory address pointer such as var b
	// and then assign it a new value

	*b = 27        // Reads as: The value at address var b change it to 27
	fmt.Println(a) // Since var b is a pointer to the mem address used by var a
	// when the value at the mem address is changed the value of var a is changed

}
