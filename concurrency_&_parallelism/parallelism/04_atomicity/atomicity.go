/*
Package atomic provides low-level atomic memory primitives
useful for implementing synchronization algorithms.

These functions require great care to be used correctly.
Except for special, low-level applications, synchronization
is better done with channels or the facilities of the sync
package. Share memory by communicating; don't communicate by
sharing memory.
*/

package main

import (
	"fmt"
	"math/rand"
	"sync"
	"sync/atomic"
	"time"
)

var wg sync.WaitGroup
var counter int64

func main() {
	wg.Add(2)
	go incrementor("Foo:")
	go incrementor("Bar:")
	wg.Wait()
	fmt.Println("Final Counter:", counter)
}

func incrementor(s string) {

	for i := 0; i < 20; i++ {
		time.Sleep(time.Duration(rand.Intn(20)) * time.Millisecond)
		atomic.AddInt64(&counter, 1)
		fmt.Println(s, i, "Counter:", counter)
	}
	wg.Done()
}
