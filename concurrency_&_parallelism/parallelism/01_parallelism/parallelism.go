/*
The only difference between running this concurrently and
parallel is addign the func init() and telling the program
how many CPUs to use at runtime.
*/

package main

import (
	"fmt"
	"runtime"
	"sync"
	"time"
)

var wg sync.WaitGroup

// Tells the program to use all the processor cores
// to specify how many processors to use replace
// runtime.GOMAXPROCS(runtime.NumCPU())
// with
// runtime.GOMAXPROCS(n) where is is the number of cores to use
func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}

func main() {
	wg.Add(2)
	go foo()
	go bar()
	wg.Wait()
}

func foo() {
	defer wg.Done()

	for i := 0; i < 50; i++ {
		fmt.Println("foo: ", i)
		time.Sleep(time.Duration(3 * time.Millisecond))
	}
}

func bar() {
	defer wg.Done()

	for i := 0; i < 50; i++ {
		fmt.Println("bar: ", i)
		time.Sleep(time.Duration(20 * time.Millisecond))
	}
}
