/*
In computer programming, a mutex (mutual exclusion object) is
a object that is created so that multiple program threads can
take turns sharing the same resource, such as access to a file
*/

package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

var wg sync.WaitGroup
var counter int
var mutex sync.Mutex

func main() {
	wg.Add(2)
	go incrementor("Foo:")
	go incrementor("Bar:")
	wg.Wait()
	fmt.Println("Final Counter:", counter)
}

func incrementor(s string) {

	for i := 0; i < 20; i++ {
		time.Sleep(time.Duration(rand.Intn(20)) * time.Millisecond)

		// this will lock the var so any other threads that may
		// want to get to this var will be unable to and will
		// have to wait until the mutex.Unlock has released it.
		mutex.Lock() // lock the resources beneath it
		counter++
		fmt.Println(s, i, "Counter:", counter)
		mutex.Unlock() // lock the resources above it
	}
	wg.Done()
}
