/*
Very basic example of using channels
*/

package main

import (
	"fmt"
)

func main() {
	c := make(chan int) // make must be used to initialze a channel

	go func() {
		for i := 0; i < 10; i++ {
			// when output of i is generated pass value through the channel
			c <- i
		}
		// when the above is done putting values onto the channel close it
		// you can continue to receive from a closed channel but nothing
		// else can be put onto it
		close(c)
	}()

	// this will continue looping over the channel and printing the values
	// that are put into it until that channel is empty.
	for n := range c {
		fmt.Println(n)
	}
}
