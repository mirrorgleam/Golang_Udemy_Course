/*
Starts 10 anonymous go functions which loop and output 0-9
*/

package main

import (
	"fmt"
)

func main() {
	n := 10
	c := make(chan int)
	done := make(chan bool)

	// loop n times and create n anonymous go funcs
	for i := 0; i < n; i++ {
		go func() {
			for i := 0; i < 10; i++ {
				c <- i
			}
			done <- true
		}()
	}

	// loop and close n number of go funcs created above
	go func() {
		for i := 0; i < n; i++ {
			<-done
		}
		close(c)
	}()

	// take n number of inputs from above funcs and print
	for n := range c {
		fmt.Println(n)
	}
}
