/*
Semaphore are a very general synchronization mechanism
that can be used to implement mutexes, limit access
to multiple resources, solve the readers-writers problem etc.

There is no semaphore implementation in Go's sync package,
but they can be emulated easily using buffered channels:
- The capacity of the buffered channel is the number
	of resourceswe wish to synchronize
- The length (number of elements currently stored) of the
	channelis the number of resources currently being used
- The capacity minus the length of the channel is the number of
	free resources (the integer value of traditional semaphores)
*/

package main

import (
	"fmt"
)

func main() {
	c := make(chan int)
	done := make(chan bool) // create another channel named done

	go func() {
		for i := 0; i < 10; i++ {
			c <- i
		}
		done <- true // when this function is complete it will set done to true
	}()

	go func() {
		for i := 0; i < 10; i++ {
			c <- i
		}
		done <- true // when this function is complete it will set done to true
	}()

	go func() {
		<-done   // this step in the func will not be
		<-done   // passed until the var is updated above
		close(c) // once the two done vars are update this will close channel c
	}()

	for n := range c {
		fmt.Println(n)
	}
}
