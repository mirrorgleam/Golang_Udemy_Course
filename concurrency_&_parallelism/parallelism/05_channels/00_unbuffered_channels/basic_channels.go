/*
Very basic example of using channels
*/

package main

import (
	"fmt"
	"time"
)

func main() {
	c := make(chan int) // make must be used to initialze a channel

	go func() {
		for i := 0; i < 10; i++ {
			// when output of i is generated pass value through the channel
			c <- i
		}
		// to pass data into an anonymous function it must be passed into the
		// execution parenthesis at the end
	}()

	go func() {
		for {
			// when the value is passed into the channel above it will
			// be output here which will print the value
			fmt.Println(<-c)
		}
	}()

	// rather than creating wait groups which will add much more code
	// to the program this will cause the program to sleep for 1 second
	// before main() exits and closes the program
	time.Sleep(time.Second)
}
