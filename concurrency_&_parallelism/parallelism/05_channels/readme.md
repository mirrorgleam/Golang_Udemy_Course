Channels are the pipes that connect concurrent goroutines.  
You can send values into channels from one goroutine and  
receive those values into another goroutine.

Go encourages an approach in which share values are passed  
around on channels and are never actively shared by separate  
threads of execution. Only one go routine has access to the  
value at any given time. Data races cannot occur, by design.

Go proverb:  
Do not communicate by sharing memory; instead,
share memory by communicating


https://gobyexample.com/channels