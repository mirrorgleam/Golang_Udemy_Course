/*
This will not work
There are three go routines created in this code; main, foo, and bar
main starts foo and bar then exits because main has completed it's task
since main exits the program closes without allowing foo and bar to
complete their tasks. We need to add wait groups to keep main from closing.
*/

package main

import "fmt"

func main() {
	go foo()
	go bar()
}

func foo() {
	for i := 0; i < 50; i++ {
		fmt.Println("foo: ", i)
	}
}

func bar() {
	for i := 0; i < 50; i++ {
		fmt.Println("bar: ", i)
	}
}
