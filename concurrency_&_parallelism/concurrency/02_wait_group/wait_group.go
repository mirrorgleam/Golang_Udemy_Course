/*
By adding wait groups from the sync package we can tell main to
not close untle the wait groups are completed with their tasks.
We are not able to see this effect since both functions take
less than a microsecond to complete. To see the effects of
concurrency we need to add a sleep operation to the functions.
*/

package main

import (
	"fmt"
	"sync"
)

var wg sync.WaitGroup

func main() {
	wg.Add(2) // add 2 wg because we have 2 functions that need to run
	go foo()
	go bar()
	wg.Wait()
}

func foo() {
	defer wg.Done() // defer signaling wg done until the function is done

	for i := 0; i < 50; i++ {
		fmt.Println("foo: ", i)
	}
}

func bar() {
	defer wg.Done() // defer signaling wg done until the function is done

	for i := 0; i < 50; i++ {
		fmt.Println("bar: ", i)
	}
}
