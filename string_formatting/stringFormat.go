package main

import "fmt"

func main() {

	fmt.Println(42)

	fmt.Println("Decimal\tBinary\tHex\tHex w/0X\tASCII")

	// Printf "format" %d Decimal, %b Binary, %X Hexadecimal, %#X Hexadecimal with 0X
	fmt.Printf("%d\t%b\t%X\t%#X\t%q\n", 42, 42, 42, 42, 42)
	fmt.Println(greet("Jane ", "Doe "))
	aString := "this string"
	anInt := 3456
	aFloat := 43.333

	toCombineThem := fmt.Sprintf("%s %d %f", aString, anInt, aFloat)

	fmt.Printf("\"%s\" TYPE: %T", toCombineThem, toCombineThem)
}

func greet(fname, lname string) (string, string) {
	return fmt.Sprint(fname, lname), fmt.Sprint(lname, fname)
}
