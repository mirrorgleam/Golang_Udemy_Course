package main

import (
	"fmt"
)

func hello() {
	fmt.Print("hello ")
}

func world() {
	fmt.Println("world")
}

// defer keyword will defer the execution of associated
// function until just before the enclosing function exits
func main() {
	defer world()
	hello()
}
