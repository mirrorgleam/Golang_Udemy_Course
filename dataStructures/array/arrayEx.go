// An array is a numbered sequence of elements of a single type
// The number of elements is called the length and is never negative
// The length is part of the array's type and must be declaired
// Not dynamic. Does not change in size

package main

import (
	"fmt"
)

func main() {
	// create var array of length 58 named x
	var x [58]string
	fmt.Println(&x)
	fmt.Println(len(x))
	fmt.Println(x[42])

	// in ascii 65 is where alphabet begins
	for i := 65; i <= 122; i++ {
		// using i - 65 to begin assignment at index 0
		// string will convert the int to ascii??
		x[i-65] = string(i)
	}
	fmt.Println(x)
	fmt.Println(len(x))
	fmt.Println(x[42])
	fmt.Println(x[4:8])      // get range of items in array; from 4 up to 8
	fmt.Println(x[:8])       // get first 8 items in the array
	fmt.Println(x[8:])       // get all items in the array after 8
	fmt.Println(x[len(x)-1]) // get last item in an array

	// string will convert the int to ascii!!
	fmt.Println(string(1254))

	// using arrays in a for loop
	for i, v := range x { // i is the index and v is the value
		fmt.Printf("%v - %T - %d\n", v, v, i)
		if i > 20 {
			break
		}
	}
}
