package main

import (
	"fmt"
)

// interface can allow for polymorphism in golang
// polymorphism is where multiple functions can use the same function name
// as long as they return the same type

// income is an interface that contains two methods calculate and source
type income interface {
	calculate() int
	source() string
}

// fixedBilling has two fields
type fixedBilling struct {
	projectName  string // the name of the project
	biddedAmount int    // the ammount that has been bid for a project
}

// timeAndMaterial has three fields
type timeAndMaterial struct {
	projectName string // the name of the project
	noOfHours   int    // the number of hours worked on a project
	hourlyRate  int    // the hourly rate for a project
}

// first calculate function will be used
// if the struct being passed is fixedBilling
func (fb fixedBilling) calculate() int {
	return fb.biddedAmount
}

// first source function will be used if
// the struct being passed is fixedBilling
func (fb fixedBilling) source() string {
	return fb.projectName
}

// second calculate function will be used
// if the struct being passed is timeAndMaterial
func (tm timeAndMaterial) calculate() int {
	return tm.noOfHours * tm.hourlyRate
}

// second source function will be used if
// the struct being passed is timeAndMaterial
func (tm timeAndMaterial) source() string {
	return tm.projectName
}

// calculateNetincome will accept a slice of type income
// and will print values to stdout
func calculateNetincome(ic []income) {
	var netincome int
	for _, income := range ic {
		fmt.Printf("income From %s = $%d\n", income.source(), income.calculate())
		netincome += income.calculate()
	}
	fmt.Printf("Net income of organisation = $%d", netincome)
}

func main() {
	// create structs with usable data
	project1 := fixedBilling{projectName: "Project 1", biddedAmount: 5000}
	project2 := fixedBilling{projectName: "Project 2", biddedAmount: 10000}
	project3 := timeAndMaterial{projectName: "Project 3", noOfHours: 160, hourlyRate: 25}
	project4 := timeAndMaterial{projectName: "Wild Horses", noOfHours: 240, hourlyRate: 30}

	// then create a slice of type income containing those structs
	incomeStreams := []income{
		project1,
		project2,
		project3,
		project4,
	}

	// pass []income to calculateNetincome function
	calculateNetincome(incomeStreams)
}
