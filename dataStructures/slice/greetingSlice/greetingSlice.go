package main

import (
	"fmt"
)

func main() {
	greeting := []string{
		"Good morning!",
		"Bonjour!",
		"dias!",
	}

	moreGreeting := []string{
		"Bonjiorno!",
		"Ohayo!",
		"Selamat pagi!",
		"Gutten morgen!",
	}

	for i, currentEntry := range greeting {
		fmt.Println(i, currentEntry)
	}

	// Append a slice to another slice ...
	fmt.Println("\nAppend slice to a slice... ")
	greeting = append(greeting, moreGreeting...)

	for j := 0; j < len(greeting); j++ {
		fmt.Println(greeting[j])
	}

	// Delete from a slice...
	fmt.Println("\nDelete from a slice... ")
	greeting = append(greeting[:2], greeting[3:]...) // Gives 0, 1, 3, 4, ...

	for j := 0; j < len(greeting); j++ {
		fmt.Println(greeting[j])
	}
}

/* OUTPUT:
0 Good morning!
1 Bonjour!
2 dias!
Good morning!
Bonjour!
dias!
Bonjiorno!
Ohayo!
Selamat pagi!
Gutten morgen!
*/
