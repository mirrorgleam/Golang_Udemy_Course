package main

import (
	"fmt"
)

func main() {
	mySlice := []string{"a", "b", "c", "d", "e", "f"} // Declare a var named mySlice of type (Slice String)
	fmt.Println(mySlice)                              // Print all values in slice
	fmt.Println(mySlice[2:4])                         // Print values from index 2 up to but not including index 4
	fmt.Println(mySlice[2])                           // Print value of index 2
	fmt.Println(string("myString"[2]))                // slice string and return value at index 2
	fmt.Println("myString"[2])                        // Slice string and return ascii int value of index 2

	// Declare empty var intSlice type (Slice int) len: 0 , cap: 5
	intSlice := make([]int, 0, 5)

	fmt.Println("-----------------------")
	fmt.Println(intSlice)
	fmt.Println(len(intSlice))
	fmt.Println(cap(intSlice))
	fmt.Println("-----------------------")

	// Append value to intSlice
	for i := 0; i < 80; i++ {
		intSlice = append(intSlice, i)
		fmt.Println("Len:", len(intSlice), "Cap:", cap(intSlice), "Value:", intSlice[i])
	}

}
