// Test current working knowledge of Golang maps and slices
// Create a slice of int and use it to build a map

package main

import (
	"fmt"
	"math/rand"
)

func main() {

	// Create new empty slice of int
	var intSlice []int

	// append 10 random int to slice
	for i := 0; i < 10; i++ {
		intSlice = append(intSlice, rand.Intn(200))
	}
	fmt.Println(intSlice)

	// Create new empty map of int keys to string values
	myMap := make(map[int]string)

	// Use intSlice values to populate key:value of myMap
	for i := 0; i < len(intSlice); i++ {
		myMap[intSlice[i]] = string(intSlice[i])
	}
	fmt.Println(myMap)

	// Print on new line k;v pairs for myMap
	for i := 0; i <= len(myMap); i++ {
		fmt.Println(intSlice[i], ":", myMap[intSlice[i]])
	}
}
