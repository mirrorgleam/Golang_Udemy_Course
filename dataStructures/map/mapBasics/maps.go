package main

import (
	"fmt"
)

func main() {
	m := make(map[string]int) // make a map of string:int [key:value]

	m["k1"] = 7  // add key:value of "k1":7 to map
	m["k2"] = 34 // add key:value of "k2":34 to map

	fmt.Println("map:", m) // print out map keys and values

	v1 := m["k1"] // assign value of key "k1" to var v1
	fmt.Println("v1:", v1)

	fmt.Println("len:", len(m)) // print length of map

	delete(m, "k2") // delete key "k2" from map m
	fmt.Println("map:", m)

	// also known as the comma okay idiom| value, exists := map["key"]
	_, ok := m["k2"]       // check for presence of key "k2"
	fmt.Println("ok:", ok) // this will return bool

	_, ok2 := m["k1"]        // same as above but returns true
	fmt.Println("ok2:", ok2) // since k1 is present in the map

	n := map[string]int{"foo": 12, "bar": 59} // you can also declare and initialize a new
	fmt.Println("map:", n)                    // map in the same line with this syntax.

}
