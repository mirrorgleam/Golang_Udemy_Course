// Marshal and unmarshal are used for converting JSON and Struct data
package main

import (
	"encoding/json"
	"fmt"
)

// Person contains parameters for person
type Person struct {
	First string
	Last  string
	Age   int
}

func main() {
	// Create p1 and print contents to demo that it's empty
	var p1 Person
	fmt.Println(p1.First)
	fmt.Println(p1.Last)
	fmt.Println(p1.Age)

	// Create JSON data with same field structure as Person struct
	bs := []byte(`{"First":"James", "Last":"Bond", "Age":20}`)
	// Write JSON data to address of p1
	json.Unmarshal(bs, &p1)

	// Print new values of p1 showing that the JSON has been written
	// to the p1 person struct
	fmt.Println("-----------")
	fmt.Println(p1.First)
	fmt.Println(p1.Last)
	fmt.Println(p1.Age)
	fmt.Printf("%T \n", p1)

}
